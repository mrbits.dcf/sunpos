#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import re


def present_seconds(seconds):
    """Strip off zeros or return nothing if zero.

    Args:
        seconds (string or int): seconds

    Returns:
        float: seconds
    """

    if seconds == 0:
        seconds = ''
    elif seconds - int(seconds) == 0:
        seconds = "%d" % int(seconds)
    else:
        seconds = "%2.2f" % seconds
        seconds = seconds.strip('0')
    return seconds


def latlong_float_conversion(latitude, longitude):
    """Convert latitude and longitude in decimal degrees into DMS Sexagesimal notation.

    Args:
        latitude (float): Latitude in decimal degrees: -23.458765
        longitude (float): Longitude in decimal degress: -46.675823

    Returns:
        String: Latitude and longitude in DMS
    """

    lat = 'N' if latitude >= 0 else 'S'
    lon = 'E' if longitude >= 0 else 'W'

    latitude = abs(latitude)
    longitude = abs(longitude)
    lat_d = int(latitude)
    lat_mf = 60 * (latitude - lat_d)
    lat_m = int(lat_mf)
    lat_s = 60 * (lat_mf - lat_m)

    lon_d = int(longitude)
    lon_mf = 60 * (longitude - lon_d)
    lon_m = int(lon_mf)
    lon_s = 60 * (lon_mf - lon_m)

    lat_s = present_seconds(lat_s)
    lon_s = present_seconds(lon_s)

    res = "%d°%d'%s%s %d°%d'%s%s" % (lat_d, lat_m, lat_s, lat, lon_d, lon_m, lon_s, lon)
    return res


def parse_digits(value):
    """Get coordinates in DMS Sexagesimal notation and extract digits.

    Args:
        value (string): Latitude or longitude in DMS Sexagesimal notation.

    Returns:
        float: only digits
    """

    expr = "^([\d]+[\d\.]*)[^\d][\s]*([\d]+[\d\.]*)[^\d]*([\d\.]*)"
    groups = []

    m = re.match(expr, value, re.I | re.M)
    try:
        groups = m.groups()
    except Exception:
        pass
    print(groups)
    res = [a if a else 0 for a in groups]
    for i in range(len(res)):
        val = float(res[i])
        if val - int(val) == 0:
            res[i] = int(res[i])
        else:
            res[i] = val
    return res


def latlong_str_conversion(latlong):
    latlong = latlong.upper()
    lat = lon = False
    n = latlong.find('N')
    s = latlong.find('S')
    e = latlong.find('E')
    w = latlong.find('W')

    if n > 0 or s > 0:
        lat = latlong[:n + s + 1].strip()
        if e > 0 or w > 0 and n + s + 2 <= len(latlong):
            lon = latlong[n + s + 2:e + w + 1].strip()
    lat_tuple = parse_digits(lat)
    lon_tuple = parse_digits(lon)
    latitude = lat_tuple[0] + lat_tuple[1] / 60.0 + lat_tuple[2] / 3600.0
    if s > 0:
        latitude = -latitude
    longitude = lon_tuple[0] + lon_tuple[1] / 60.0 + lon_tuple[2] / 3600.0
    if w > 0:
        longitude = -longitude
    return (latitude, longitude)


def leapyear(year):
    if year % 400 == 0:
        return True
    elif year % 100 == 0:
        return False
    elif year % 4 == 0:
        return True
    else:
        return False


def leapYearsSince1949(year):
    leapYearsTo1949 = 472
    leapDaysInAYear = 0.2425
    leapYearsToGivenYear = int(year * leapDaysInAYear)
    leapYearsBetweenYears = leapYearsToGivenYear - leapYearsTo1949
    return leapYearsBetweenYears


def calc_time(year, month, day, hour=12, minute=0, sec=0):
    month_days = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30]
    day = day + sum(month_days[:month])
    leapdays = leapyear(year) and day >= 60 and (not (month == 2 and day == 60))
    if leapdays:
        day += 1

    hour = hour + minute / 60.0 + sec / 3600.0
    delta = year - 1949
    leap = leapYearsSince1949(year)
    jd = 32916.5 + (delta * 365) + leap + day + (hour / 24.0)
    time = jd - 51545
    return time


def meanLongitudeDegrees(time):
    return ((280.460 + 0.9856474 * time) % 360)


def meanAnomalyRadians(time):
    return (math.radians((357.528 + 0.9856003 * time) % 360))


def eclipticLongitudeRadians(mnlong, mnanomaly):
    return (math.radians((mnlong + 1.915 * math.sin(mnanomaly) + 0.020 * math.sin(2 * mnanomaly)) % 360))


def eclipticObliquityRadians(time):
    return (math.radians(23.439 - 0.0000004 * time))


def rightAscensionRadians(oblqec, eclong):
    num = math.cos(oblqec) * math.sin(eclong)
    den = math.cos(eclong)
    ra = math.atan(num / den)
    if den < 0:
        ra += math.pi
    if (den >= 0 and num < 0):
        ra += 2 * math.pi
    return (ra)


def rightDeclinationRadians(oblqec, eclong):
    return (math.asin(math.sin(oblqec) * math.sin(eclong)))


def greenwichMeanSiderealTimeHours(time, hour):
    return ((6.697375 + 0.0657098242 * time + hour) % 24)


def localMeanSiderealTimeRadians(gmst, longitude):
    return (math.radians(15 * ((gmst + longitude / 15.0) % 24)))


def hourAngleRadians(lmst, ra):
    return (((lmst - ra + math.pi) % (2 * math.pi)) - math.pi)


def elevationRadians(lat, dec, ha):
    return (math.asin(math.sin(dec) * math.sin(lat) + math.cos(dec) * math.cos(lat) * math.cos(ha)))


def solarAzimuthRadians(lat, dec, ha):
    zenithAngle = math.acos(math.sin(lat) * math.sin(dec) + math.cos(lat) * math.cos(dec) * math.cos(ha))
    az = math.acos((math.sin(lat) * math.cos(zenithAngle) - math.sin(dec)) / (math.cos(lat) * math.sin(zenithAngle)))
    if ha > 0:
        az = az + math.pi
    else:
        az = (3 * math.pi - az) % (2 * math.pi)
    return (az)


def sun_position(year, month, day, hour=12, minute=0, sec=0, lat=0.0, longitude=0.0):
    """Returns Sun azimuth and elevation for given latitude, longitude, date and time.

    Parameters:
        year (int): Year
        month (int): Month
        day (int): Day
        hour (int): Hour
        minute (int): Minute
        sec (int): Second
        lat (float): Latitude in decimal degrees
        longitude (float): Longitude in decimal degrees

    Returns:
        float: Sun azimuth and elevation
    """

    time = calc_time(year, month, day, hour, minute, sec)
    hour = hour + minute / 60.0 + sec / 3600.0
    mnlong = meanLongitudeDegrees(time)
    mnanom = meanAnomalyRadians(time)
    eclong = eclipticLongitudeRadians(mnlong, mnanom)
    oblqec = eclipticObliquityRadians(time)

    ra = rightAscensionRadians(oblqec, eclong)
    dec = rightDeclinationRadians(oblqec, eclong)

    gmst = greenwichMeanSiderealTimeHours(time, hour)
    lmst = localMeanSiderealTimeRadians(gmst, longitude)

    ha = hourAngleRadians(lmst, ra)
    lat = math.radians(lat)
    el = elevationRadians(lat, dec, ha)
    az = solarAzimuthRadians(lat, dec, ha)

    elevation = math.degrees(el)
    azimuth = math.degrees(az)

    return (azimuth, elevation)


if __name__ == '__main__':
    samples = [(46.5, -6.5, 163.03, 65.83),
               (46.0, -6.0, 163.82, 66.41),
               (-41, 0, 0.98, 25.93),
               (-3, 0, 2.01, 63.9),
               (3, 0, 2.58, 69.89),
               (41, 0, 177.11, 72.07),
               (40, 0, 176.95, 73.07),
               (-40, 0, 0.99, 26.93),
               (-40, -40, 38.91, 16.31),
               (-40, 40, 322.67, 17.22),
               (-20, 100, 289.35, -15.64),
               (20, -100, 64.62, -1.55),
               (80, 100, 283.05, 21.2),
               (80, 20, 200.83, 32.51),
               (80, 0, 178.94, 33.11),
               (80, -40, 135.6, 30.47),
               (80, -120, 55.89, 17.74),
               (0, 0, 2.26, 66.89)
               ]
    print("Noon July 1 2014 at 0,0 = 2.26, 66.89")
    print("", sun_position(2014, 7, 1, lat=0, longitude=0))
    print("")
    print("Noon Dec 22 2012 at 41,0 = 180.30, 25.6")
    print("", sun_position(2012, 12, 22, lat=41, longitude=0))
    print("")
    print("Noon Dec 22 2012 at -41,0 = 359.08, 72.43")
    print("", sun_position(2012, 12, 22, lat=-41, longitude=0))
    print("")

    for s in samples:
        lat, lon, az, el = s
        print("\nFor lat,long:", lat, lon,)
        calc_az, calc_el = sun_position(2014, 7, 1, lat=lat, longitude=lon)
        az_ok = abs(az - calc_az) < 0.5
        el_ok = abs(el - calc_el) < 0.5
        if not(az_ok and el_ok):
            print("")
            print("Azimuth (Noaa,calc) = %4.2f %4.2f (error %4.2f)" % (az, calc_az, abs(az - calc_az)))
            print("Elevation (Noaa,calc) = %4.2f %4.2f (error %4.2f)" % (el, calc_el, abs(el - calc_el)))
        else:
            print(" OK (<0.5 error)")
