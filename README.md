SunPos
======

Dadas as coordenadas geogr�ficas, data e hora em GMT, calculamos o azimute e eleva��o do Sol.

Extensivamente testado contra o site da NOOA e suncalc.org.

http://www.esrl.noaa.gov/gmd/grad/solcalc/azel.html
http://suncalc.org

## Pontos importantes para se notar:
* Latitude � definida como +-90 graus onde 0 � a linha do Equador e +90 � o Polo Norte.

* Longitude � definida como +-180 graus � partir da linha de GMT, mas temos duas representa��es:
  - Modo _mapa_ onde valores positivos indicam coordenadas a Leste;
  - Modo _sat�lite_ onde valores positivos indicam coordenadas a Oeste.

* O site da NOOA - onde podemos validar a sa�da desse c�digo - usa o modo _sat�lite_.
   - http://www.esrl.noaa.gov/gmd/grad/solcalc/azel.html
   
* A grande maioria dos mapas e este c�digo usam o modo _mapa_.

## Entrada de dados:
* Uma latitude ou longitude com 8 casas decimais aponta para uma localiza��o com resolu��o de 1 mil�metro.
* Latitudes ao Sul e Longitudes a Oeste deve ser precedidas por um sinal de *menos*

* At� o presente momento, as coordenadas deve ser indicadas em graus decimais.


* Onde encontrar:
   - http://www.findlatitudeandlongitude.com/
   
* Exemplos 
   - Lisboa.
   Latitude: 38.72409�
   Longitude: -9.140625�

   - Lima
   Latitude:-11.953349�
   Longitude:-76.992187�

Uso:
```
lat = -23.5505
lon = -46.6333
year = 2019
month = 1
day = 1
hour = 12
minute = 0
sec = 0

# retorna azimute e eleva��o para o meio-dia (GMT) de 01/01/2019
sun_position(year, month, day, lat, lon)

# se necessitamos de outro hor�rio, indicamos
sun_position(year, month, day, hour, min, sec, lat, lon)
```

Para uso com Docker 

* Build
```
docker build -t sunpos .
```

* Run
```
docker run --rm -it sunpos /bin/sh
```

Uma vez dentro do container, fa�a:

```
ipython
```

Dentro da ferramenta, fa�a:

```
In [1]: import sunpos

In [2]: In [2]: sunpos.sun_position?
Signature: sunpos.sun_position(year, month, day, hour=12, minute=0, sec=0, lat=0.0, longitude=0.0)
Docstring:
Returns Sun azimuth and elevation for given latitude, longitude, date and time.

Parameters:
    year (int): Year
    month (int): Month
    day (int): Day
    hour (int): Hour
    minute (int): Minute
    sec (int): Second
    lat (float): Latitude in decimal degrees
    longitude (float): Longitude in decimal degrees

Returns:
    float: Sun azimuth and elevation
File:      ~/projects/mrbits/sun-angle/sunpos.py
Type:      function

In [3]: sunpos.sun_position(2019, 1, 1, 11, 45, 0, -23.3, -46.6)
Out[3]: (100.32934740681554, 43.17275038896316)
```

ToDO: Ajustar fun��es de convers�o de nota��o NSEW para graus decimais e novamente para NSEW.

Como contribuir:
* Fa�a fork desse projeto.
* Modifique-o � vontade.
* Submeta merge requests.
* Spread some love.

Licen�a: Veja LICENSE.md
