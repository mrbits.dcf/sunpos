FROM python:alpine

WORKDIR /app

COPY . /app

RUN pip install -U pip && pip install -r requirements.txt
